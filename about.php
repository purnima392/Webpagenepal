<?php include('header.php')?>
<section class="intorSection gray_bg">
  <div class="container custom-container">
    
    <div class="content_block">
      <div class="row">
        <div class="col-lg-6">
        <div class="introTitle">
      <h2>About Us</h2>
    </div>
          <p>Webpage Nepal is professional IT services and solutions Company located in Pokhara, Kaski, Nepal, providing high profile services at competitive rates. We are a fast growing website design company that develops high-end internet portals and websites used by hundreds of thousands of people around the world.</p>
          <p>Webpage Nepal is the leading web design company offers professional website design and development services, custom web application development and custom eCommerce solutions. We are the web designing/web development professional with excellent graphic web designing skills complying to W3C standards. Our talented team of dedicated professionals will bring together your vision and ideas into reality with creativity, technology and marketing. Webpage Nepal has been involved in diverse types of design and development projects including content management, social networks and eCommerce implementations.</p>
          <p>Our services span all aspects of website design, online marketing and customized application development for clients based on their requirements and area of operation. We offer enterprise solutions for all business processes that can be customized as required. Our aim is to deliver customer satisfaction by providing value-added, innovative and technically superior solutions to clients and help them improve their business processes.</p>
        </div>
         <div class="col-lg-6">
         	<img src="img/hr-serv.png" alt="">
         </div>
      </div>
    </div>
    <div class="content_block">
      <div class="row">
        <div class="col-lg-6 why_choose_us">
          <div class="sub-title">
            <h4>Why Choose Us</h4>
            <div class="webpage-logo"><img src="img/logo.png" alt=""></div>
          </div>
          <ul>
            <li><b>We're Affordable:</b> Our streamlined website development process and highly skilled website designers allows us to provide our professional website design at very competitive rates.</li>
            <li><b>We are Friendly:</b> Most of our clients have fun working with us. We make the web design process easy and pleasant.</li>
            <li><b>We're Honest:</b> Our business has grown word-of-mouth and we intend to continue growing this way. We'll treat you fairly and do a great job so that you'll want to continue working with us after your site has launched. And, we want you to tell all your friends and colleagues about us.</li>
            <li><b>We Emphasize Simplicity:</b>Some firms love to build complicated sites. Not us. We choose the simplest tool that will effectively accomplish a task. Simple sites load faster, are easy to use and are less prone to problems.</li>
            <li><b>We Build Powerful Websites:</b>If you need a site with all the bells and whistles, contact Webpage Nepal. Database integration, video, audio, ios and android integration, e-commerce – no problem.</li>
            <li><b>You’re the Boss:</b>We’ll give you options and the tools and information to make good choices. We’ll give you our recommendations, but ultimately, every decision about your website is yours.</li>
            <li><b>You will Love your Design:</b>Our number one focus when we design your new website is to make sure you love it. We work together with you to come up with the perfect website look and feel that best suits your business. You know your business, your customers and your branding better than anyone so we make sure you are involved every step of the way.</li>
          </ul>
        </div>
        <div class="col-lg-6 why_choose_us">
          <div class="sub-title">
            <h4>Frequenlty Ask Question(FAQ)</h4>
           
          </div>
          <div id="accordion" role="tablist">
  <div class="card">
    <div class="card-header" role="tab" id="headingOne">
      <h5 class="mb-0">
        <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          How do I determine the county average wage or the state average wage?
        </a>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        The county average wage and statewide average wage is calculated by the Department of Economic Development and can be found at www.missourieconomy.org/indicators/countywage.stm. The statewide average wage is listed last on the table.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="headingTwo">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Where can I get my corporate kit or corporate seal?
        </a>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
        Corporate kits may be purchased from a legal stationery store. A corporate kit usually contains a corporate seal, blank stock certificates and forms for the adoption of by-laws and recording the minutes of meetings. Rather than composing their own forms, some may find it easier to use the preprinted forms provided in a corporate kit. Please note that New York State law does not require a corporation to have a seal. Your telephone book’s yellow pages or a yellow pages information operator (your area code + 555-1212) may be helpful in locating a legal stationery store.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="headingThree">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Does Missouri require depreciation to be added back?
        </a>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        No. The provision requiring the adding back of bonus depreciation includes property purchased on or after July 1, 2002 but before July 1, 2003, in determining if the bonus depreciation must be used as a Missouri modification. Property purchased before July 1, 2002 and after June 30, 2003, does not qualify for the modification.
      </div>
    </div>
  </div>
   <div class="card">
    <div class="card-header" role="tab" id="headingFour">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
          What if my Missouri due date falls before the federal due date?
        </a>
      </h5>
    </div>
    <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour" data-parent="#accordion">
      <div class="card-body">
        You must file and pay an estimated return by the original Missouri due date and when the federal return is completed, file an amended Missouri return.
      </div>
    </div>
  </div>
    <div class="card">
    <div class="card-header" role="tab" id="headingFive">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
          How does a foreign corporation apply for authority to conduct business in New York State?
        </a>
      </h5>
    </div>
    <div id="collapseFive" class="collapse" role="tabpanel" aria-labelledby="headingFive" data-parent="#accordion">
      <div class="card-body">
        A foreign business corporation may apply for authority to do business in the State of New York by filing an Application for Authority pursuant to Section 1304 of the Business Corporation Law.
      </div>
    </div>
  </div>
   <div class="card">
    <div class="card-header" role="tab" id="headingSix">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapseSix" aria-expanded="false" aria-controls="collapseThree">
          How do I obtain a license or permit for my business entity?
        </a>
      </h5>
    </div>
    <div id="collapseSix" class="collapse" role="tabpanel" aria-labelledby="headingSix" data-parent="#accordion">
      <div class="card-body">
        Once the business entity is formed or registered with the California Secretary of State it must obtain the necessary licenses and/or permits. The Secretary of State does not issue licenses or permits for business entities. Please refer to the CalGold <b>(California Government: On–Line to Desktops)</b> website for information about business license/permit requirements. CalGold’s online database provides links and contact information to agencies that administer and issue business licenses, permits and registration requirements from all levels of government.
      </div>
    </div>
  </div>
     <div class="card">
    <div class="card-header" role="tab" id="headingSeven">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapseSeven" aria-expanded="false" aria-controls="collapseThree">
          What is a Professional Service Corporation?
        </a>
      </h5>
    </div>
    <div id="collapseSeven" class="collapse" role="tabpanel" aria-labelledby="headingSeven" data-parent="#accordion">
      <div class="card-body">
        One or more professionals may form, or cause to be formed, a professional service corporation (P.C.) for pecuniary profit for the purpose of rendering the professional service or services that the professionals are authorized to practice. A P.C. is formed by filing a Certificate of Incorporation pursuant to Section 1503 of the Business Corporation Law. “Profession,” as defined in Section 1501(b) of the Business Corporation Law, includes the occupations regulated by Title VIII of the Education Law plus any practice as an attorney and counselor-at-law, or as a licensed physician.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="headingEight">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapseEight" aria-expanded="false" aria-controls="collapseThree">
          How do I form a business corporation?
        </a>
      </h5>
    </div>
    <div id="collapseEight" class="collapse" role="tabpanel" aria-labelledby="headingEight" data-parent="#accordion">
      <div class="card-body">
        A business corporation may be formed by filing a Certificate of Incorporation pursuant to Section 402 of the Business Corporation Law. The Department of State has prepared instructions intended for use by first-time domestic incorporation. It includes answers to commonly asked questions, names and addresses of other government agencies that a corporation may need to contact, instructions for completing a Certificate of Incorporation, tax information, and fee information.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="headingNine">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapseNine" aria-expanded="false" aria-controls="collapseThree">
          What is a business corporation?
        </a>
      </h5>
    </div>
    <div id="collapseNine" class="collapse" role="tabpanel" aria-labelledby="headingNine" data-parent="#accordion">
      <div class="card-body">
        A business corporation is a legal entity separate and distinct from the individual(s) who compose the business. It has rights and abilities similar to those of a natural person. Principal features are perpetual duration, limited liability and easy transferability of interests. A corporation may be formed for any lawful business purpose or purposes. The Department of State cannot offer advice about the choice of business formation and strongly recommends consulting with legal and financial advisors before making the decision.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="headingTen">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapseTen" aria-expanded="false" aria-controls="collapseThree">
          What types of businesses operate in New York State?
        </a>
      </h5>
    </div>
    <div id="collapseTen" class="collapse" role="tabpanel" aria-labelledby="headingTen" data-parent="#accordion">
      <div class="card-body">
        <b>Types of businesses that operate in New York State are:</b>
        <hr/>
        <div class="why_choose_us">
        	<ul>
            	<li>Business Corporation</li>
                <li>Not-for-Profit Corporation</li>
                <li>Limited Liability Company</li>
                <li>General Partnership</li>
                <li>Limited Partnership</li>
                <li>Sole Proprietorship</li>
            </ul>
        </div>
      </div>
    </div>
  </div>
</div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php include('footer.php')?>