<?php include('header.php')?>
<section class="intorSection gray_bg">
  <div class="container custom-container">
    <div class="content_block">
      <div class="introTitle">
        <h2>Contact Us</h2>
      </div>
      <div class="contact-us-three">
					
						<div class="row">
							<div class="col-md-8">
							
								<!-- Contact map -->
								<div class="contact-map">
									<!-- Map Link -->
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3515.6385244999296!2d83.98462051445631!3d28.21829300946003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3995944db95223d3%3A0xe6feb3b2f9aef520!2sWebpage+Nepal+Private+Limited!5e0!3m2!1sen!2snp!4v1509441454515" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
								</div>
								<hr />
								
								<!-- Contact Form -->
								<div class="contact-form">
									<h5>Contact Form</h5>
									<!-- Form -->
									<form class="form" role="form">
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<input type="text" class="form-control" placeholder="Enter Name">
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<input type="text" class="form-control" placeholder="Enter Email">
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<input type="text" class="form-control" placeholder="Enter Phone">
												</div>
											</div>
										</div>										
										<div class="form-group">
											<textarea class="form-control" id="comments" rows="8" placeholder="Enter Message"></textarea>
										</div>
										<!-- Button -->
										<button type="button" class="btn btn-outline-danger">Submit</button>&nbsp;
										<button type="button" class="btn btn-outline-secondary">Reset</button>
									</form>
								</div>
								
								<br />
								
							</div>
							<div class="col-md-4 col-sm-6">
								
								<div class="well">
									<h6><i class="fa fa-user"></i>Get In Touch</h6>
									<p>Please Contact us for Domain Name Registration, Web Hosting, Website Designing, Web Development, Web Programming, Website Designing & Development Training, Social Media Marketing & Bulk SMS.</p>
									<div class="brand-bg">
										<!-- Social Media Icons -->
										<a href="#" class="facebook"><i class="fa fa-facebook circle"></i></a>
										<a href="#" class="twitter"><i class="fa fa-twitter circle"></i></a>
										<a href="#" class="google-plus"><i class="fa fa-google-plus circle"></i></a>
										<a href="#" class="linkedin"><i class="fa fa-linkedin circle"></i></a>
										<a href="#" class="pinterest"><i class="fa fa-pinterest circle"></i></a>
									</div>
								</div>
								
								<div class="well">
									<!-- Heading -->
									<h6><i class="fa fa-home"></i>Webpage Nepal Private Limited</h6>
									<!-- Paragraph -->
									<p>
										BP Road, New Road, House No: 165

Pokhara-9, Kaski, Gandaki, Nepal</p>
									<p> <i class="fa fa-phone"></i><b>Telephone :</b>+977 (61) 532305<br />
									<i class="fa fa-envelope"></i><b>Mail :</b> <a href="#">info@webpagenepal.com</a><br />
									<i class="fa fa-mobile"></i><b>Mobile :</b> +977-9801126697</p>
								</div>
								
								<div class="well">
									<h6><i class="fa fa-calendar"></i>Business Hours</h6>
									<p><b>Weekdays :</b> 10:00 AM to 5:00 PM<br />
									<b>Saturday</b> : Holiday</p>
								</div>
								
							</div>
						</div>
												
					</div>
    </div>
  </div>
</section>
<?php include('footer.php')?>