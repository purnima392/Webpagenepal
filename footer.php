<!--Footer Start-->
<footer>
	<div class="top-footer">
    	<div class="container custom-container clearfix">
        	<h3 class="float-left">Follow our site through social medai</h3>
            <ul class="float-right social_media">
            	<li><a href="#"><i class="fa fa-facebook"></i><span><b>3,060</b>Followers</span></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i><span><b>1,060</b>Followers</span></a></li>
                <li><a href="#"><i class="fa fa-envelope"></i><span><b>2,060</b>Followers</span></a></li>
            </ul>
        </div>
    </div>
    <div class="bottom_footer">
    	<div class="container custom-container text-lg-center">
        	<div class="footer_title">
            	<h4>Contact Info</h4>
            </div>
            <ul class="footer_contact_info">
            	<li><i class="fa fa-map-marker"></i>Newroad, Pokhara, Nepal</li>
                <li><i class="fa fa-phone"></i>9802826697 </li>
                <li><i class="fa fa-envelope"></i><a href="#">info@webpagenepal.com </a></li>
                <li><i class="fa fa-globe"></i><a href="#">www.webpagenepal.com   </a></li>
            </ul>
            <ul class="footer_menu">
            	<li><a href="#">About Us</a></li>
                <li><a href="#">Careers</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Terms of services</a></li>
            </ul>
            <small>Copyright © 2017 <a href="#">Webpage Nepal</a>.</small>
        </div>
    </div>
</footer>
<!--Footer End-->
<!-- Optional JavaScript --> 
<!-- jQuery first, then Popper.js, then Bootstrap JS --> 
<script src="js/jquery-3.1.0.js" type="text/javascript"></script> 
<script src="js/popper.min.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript"></script> 
<!-- Modernizr javascript --> 
<script type="text/javascript" src="js/modernizr.js"></script> 
<!-- jQuery REVOLUTION Slider  --> 
<script type="text/javascript" src="js/jquery.themepunch.tools.min.js"></script> 
<script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script> 
<script src="js/aos.js" type="text/javascript"></script> 
<script src="js/isotope.pkgd.min.js" type="text/javascript"></script> 
<script src="js/script.js" type="text/javascript"></script> 
<script>
  $(document).ready(function(){
  			AOS.init({
  				easing: 'ease-in-sine',
  				duration: 1000});
  });
  		</script>
</body>
</html>