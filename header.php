<!doctype html>
<html lang="en">
<head>
<title>Webpage Nepal Pvt.Ltd</title>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
<!-- Fonts Css -->
<link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700" rel="stylesheet">
<!--Icon Fonts-->
<link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/fonts.css">
<!-- Plugins -->
<link href="css/settings.css" media="screen" rel="stylesheet">
<link href="css/extralayers.css" media="screen" rel="stylesheet">
<link href="css/aos.css" media="screen" rel="stylesheet">
<link rel="stylesheet" href="css/style.css">
<link href="css/responsive.css" media="screen" rel="stylesheet">
</head>
<body>
<!-- scrollToTop --> 
<!-- ================ -->
<div class="scrollToTop"><i class="fa fa-angle-up"></i></div>
<!-- Header Start -->
<header>
  <div class="container custom-container">
    <div class="row">
      <div class="col-lg-2">
        <h1 class="logo"><a href="index.php"><img src="img/logo.png" alt="Webpage Nepal"></a></h1>
      </div>
      <div class="col-lg-10"> 
        <!-- Top Head Start -->
        <div class="top-head">
          <div class="top-nav">
            <ul>
              <li><a href="#">Digital Marketing</a></li>
              <li><a href="#">Web Hosting</a></li>
              <li><a href="#">Domain Registration</a></li>
              <li class="nav-phone"><a href="#">+977-9801126697</a></li>
            </ul>
          </div>
        </div>
        <!-- Top Head End --> 
        <!-- Navigation Start -->
        <nav class="navbar navbar-expand-lg navbar-light"> <a class="navbar-brand" href="#">Menu</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto" id="nav">
              <li class="nav-item active"> <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a> </li>
              <li class="nav-item"> <a class="nav-link" href="about.php">About Us</a> </li>
              <li class="nav-item"> <a class="nav-link" href="our-team.php">Our Team</a> </li>
              <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Services </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown"> <a class="dropdown-item" href="#">Domain Registration</a> <a class="dropdown-item" href="#">Web Designing & Development</a> <a class="dropdown-item" href="#">Web Hosting</a> <a class="dropdown-item" href="#">Mobile Marketing</a> <a class="dropdown-item" href="#">Digital Marketing</a> <a class="dropdown-item" href="#">Training</a> </div>
              </li>
              <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Courses </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown"> <a class="dropdown-item" href="#">Computer Basic</a> <a class="dropdown-item" href="#">Diploma</a> <a class="dropdown-item" href="#">Web Design</a> <a class="dropdown-item" href="#">Graphic Design</a> <a class="dropdown-item" href="#">PHP</a> <a class="dropdown-item" href="#">Java</a> <a class="dropdown-item" href="#">Android</a> <a class="dropdown-item" href="#">MySQL</a> <a class="dropdown-item" href="#">Wordpress</a> </div>
              </li>
              <li class="nav-item"> <a class="nav-link" href="portfolio.php">Portfolio</a> </li>
              <li class="nav-item"> <a class="nav-link" href="contact.php">Contact Us</a> </li>
              <li class="nav-item work-with-us"> <a class="nav-link" href="#">Work With Us <i class="fa fa-angle-right"></i></a> </li>
            </ul>
          </div>
        </nav>
        <!-- Navigation End --> 
      </div>
    </div>
  </div>
</header>
<!-- Header End --> 