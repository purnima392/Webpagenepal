<?php include('header.php')?>
<section class="intorSection gray_bg">
  <div class="container custom-container">
    <div class="content_block">
      <div class="introTitle">
        <h2>Our Team</h2>
      </div>
      <div class="team">
        <div class="row">
          <div class="col-lg-3 col-sm-6"> 
            <!-- Team member -->
            <div class="team-member">
              <div class="t-container"> 
                <!-- Social -->
                <div class="social brand-bg"> <a class="facebook" href="#"><i class="fa fa-facebook circle"></i></a> <a class="google-plus" href="#"><i class="fa fa-google-plus circle"></i></a> <a class="twitter" href="#"><i class="fa fa-twitter circle"></i></a> <a class="linkedin" href="#"><i class="fa fa-linkedin circle"></i></a> </div>
                <!-- Image --> 
                <img class="img-responsive" src="img/pritam-sen-thakuri.jpg" alt="" /> </div>
              <!-- Name -->
              <div class="team-info">
                <h4> Pitam Bahadur Sen <span>CEO/Founder</span></h4>
                <p>Lorem ipsum ctetur dolor ctetur sit amet, conse ctetur tempor elit.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-sm-6"> 
            <!-- Team member -->
            <div class="team-member">
              <div class="t-container"> 
                <!-- Social -->
                <div class="social brand-bg"> <a class="facebook" href="#"><i class="fa fa-facebook circle"></i></a> <a class="google-plus" href="#"><i class="fa fa-google-plus circle"></i></a> <a class="twitter" href="#"><i class="fa fa-twitter circle"></i></a> <a class="linkedin" href="#"><i class="fa fa-linkedin circle"></i></a> </div>
                <!-- Image --> 
                <img class="img-responsive" src="img/rashmi-shahi.jpg" alt="" /> </div>
              <!-- Name -->
              <div class="team-info">
                <h4>Rasmi Shahi <span>Chief Operating Officer</span></h4>
                <p>Lorem ipsum ctetur dolor ctetur sit amet, conse ctetur tempor elit.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-sm-6"> 
            <!-- Team member -->
            <div class="team-member">
              <div class="t-container"> 
                <!-- Social -->
                <div class="social brand-bg"> <a class="facebook" href="#"><i class="fa fa-facebook circle"></i></a> <a class="google-plus" href="#"><i class="fa fa-google-plus circle"></i></a> <a class="twitter" href="#"><i class="fa fa-twitter circle"></i></a> <a class="linkedin" href="#"><i class="fa fa-linkedin circle"></i></a> </div>
                <!-- Image --> 
                <img class="img-responsive" src="img/purnima-sai.jpg" alt="" /> </div>
              <!-- Name -->
              <div class="team-info">
                <h4>Punima Sai <span>Sr. Web Designer</span></h4>
                <p>Lorem ipsum ctetur dolor ctetur sit amet, conse ctetur tempor elit.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-sm-6"> 
            <!-- Team member -->
            <div class="team-member">
              <div class="t-container"> 
                <!-- Social -->
                <div class="social brand-bg"> <a class="facebook" href="#"><i class="fa fa-facebook circle"></i></a> <a class="google-plus" href="#"><i class="fa fa-google-plus circle"></i></a> <a class="twitter" href="#"><i class="fa fa-twitter circle"></i></a> <a class="linkedin" href="#"><i class="fa fa-linkedin circle"></i></a> </div>
                <!-- Image --> 
                <img class="img-responsive" src="img/dipika-agrawal.jpg" alt="" /> </div>
              <!-- Name -->
              <div class="team-info">
                <h4>Dipika Agrawal<span>Sr. Web Developer</span></h4>
                <p>Lorem ipsum ctetur dolor ctetur sit amet, conse ctetur tempor elit.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-sm-6"> 
            <!-- Team member -->
            <div class="team-member">
              <div class="t-container"> 
                <!-- Social -->
                <div class="social brand-bg"> <a class="facebook" href="#"><i class="fa fa-facebook circle"></i></a> <a class="google-plus" href="#"><i class="fa fa-google-plus circle"></i></a> <a class="twitter" href="#"><i class="fa fa-twitter circle"></i></a> <a class="linkedin" href="#"><i class="fa fa-linkedin circle"></i></a> </div>
                <!-- Image --> 
                <img class="img-responsive" src="img/20150227_105016-1.jpg" alt="" /> </div>
              <!-- Name -->
              <div class="team-info">
                <h4>Sushil BK<span>Web Designer</span></h4>
                <p>Lorem ipsum ctetur dolor ctetur sit amet, conse ctetur tempor elit.</p>
              </div>
            </div>
          </div>
          
          
          
        </div>
      </div>
    </div>
  </div>
</section>
<?php include('footer.php')?>