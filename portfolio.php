<?php include('header.php')?>
<section class="work gray_bg">
  <div class="container custom-container">
    <div class="title text-lg-center">
      <h2>Our Work</h2>
    </div>
    <div id="filters" class="button-group filter-button-group">
      <button class="button btn btn-outline-secondary is-checked" data-filter="*">All</button>
      <button class="button btn btn-outline-secondary" data-filter=".web-design">Web Design</button>
      <button class="button btn btn-outline-secondary" data-filter=".graphic-design">Graphic Design</button>
      <button class="button btn btn-outline-secondary" data-filter=".wordpress">Wordpress</button>
    </div>
    <div class="grid row clearfix">
      <div class="col-lg-3 grid-item web-design" data-category="web-design" data-aos="fade-up" data-aos-duration="100">
        <div class="grid-item-list"><img src="img/nada_times.png" alt="nada Times">
          <div class="overlay-container">
            <h4>Nada Times Weekly</h4>
            <a href="#" class="btn btn-outline-secondary"><i class="fa fa-eye"></i> View Website</a> </div>
        </div>
      </div>
      <div class="col-lg-3 grid-item graphic-design" data-category="graphic-design" data-aos="fade-up" data-aos-duration="200">
        <div class="grid-item-list"><img src="img/rana-hotel.png" alt="nada Times">
          <div class="overlay-container">
            <h4>Nada Times Weekly</h4>
            <a href="#" class="btn btn-outline-secondary"><i class="fa fa-eye"></i> View Website</a> </div>
        </div>
      </div>
      <div class="col-lg-3 grid-item wordpress" data-category="wordpress" data-aos="fade-up" data-aos-duration="300">
        <div class="grid-item-list"><img src="img/pokhara-express.png" alt="nada Times">
          <div class="overlay-container">
            <h4>Nada Times Weekly</h4>
            <a href="#" class="btn btn-outline-secondary"><i class="fa fa-eye"></i> View Website</a> </div>
        </div>
      </div>
      <div class="col-lg-3 grid-item web-design" data-category="web-design" data-aos="fade-up" data-aos-duration="400">
        <div class="grid-item-list"><img src="img/sunakhkari.png" alt="nada Times">
          <div class="overlay-container">
            <h4>Nada Times Weekly</h4>
            <a href="#" class="btn btn-outline-secondary"><i class="fa fa-eye"></i> View Website</a> </div>
        </div>
      </div>
      <div class="col-lg-3 grid-item web-design" data-category="web-design" data-aos="fade-down" data-aos-duration="500">
        <div class="grid-item-list"><img src="img/nada_times.png" alt="nada Times">
          <div class="overlay-container">
            <h4>Nada Times Weekly</h4>
            <a href="#" class="btn btn-outline-secondary"><i class="fa fa-eye"></i> View Website</a> </div>
        </div>
      </div>
      <div class="col-lg-3 grid-item graphic-design" data-category="graphic-design" data-aos="fade-down" data-aos-duration="600">
        <div class="grid-item-list"><img src="img/rana-hotel.png" alt="nada Times">
          <div class="overlay-container">
            <h4>Nada Times Weekly</h4>
            <a href="#" class="btn btn-outline-secondary"><i class="fa fa-eye"></i> View Website</a> </div>
        </div>
      </div>
      <div class="col-lg-3 grid-item wordpress" data-category="wordpress" data-aos="fade-down" data-aos-duration="700">
        <div class="grid-item-list"><img src="img/pokhara-express.png" alt="nada Times">
          <div class="overlay-container">
            <h4>Nada Times Weekly</h4>
            <a href="#" class="btn btn-outline-secondary"><i class="fa fa-eye"></i> View Website</a> </div>
        </div>
      </div>
      <div class="col-lg-3 grid-item web-design" data-category="web-design" data-aos="fade-down" data-aos-duration="800">
        <div class="grid-item-list"><img src="img/sunakhkari.png" alt="nada Times">
          <div class="overlay-container">
            <h4>Nada Times Weekly</h4>
            <a href="#" class="btn btn-outline-secondary"><i class="fa fa-eye"></i> View Website</a> </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php include('footer.php')?>